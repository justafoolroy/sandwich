<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Set home
Route::get('/', 'HomeController@index' );

//

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();


//Campagnes
Route::get('/campagnes', 'CampagneController@index');
Route::get('/campagne/{id}', 'CampagneController@view');
Route::get('/edit/campagne/{id}', 'CampagneController@edit');
Route::post('/edit/campagne/{id}', 'CampagneController@update');

//Locations
Route::get('/locations', 'LocationController@index');
Route::get('/location/{id}', 'LocationController@view');
Route::get('/edit/location/{id}', 'LocationController@edit');
Route::post('/edit/location/{id}', 'locationController@update');

//Prices
Route::get('/prices', 'PriceController@index');
Route::get('/price/{id}', 'PriceController@view');
Route::get('/edit/price/{id}', 'PriceController@edit');
Route::post('/edit/price/{id}', 'PriceController@update');

//Posters for the sticker
Route::get('/posters', 'PosterController@index');


