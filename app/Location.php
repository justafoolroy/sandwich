<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {
	
	public function campagnes()
	{
		return $this->belongsToMany('App\Campagne');
	}
	
	public function price()
	{
		return $this->belongsTo( 'App\Price' );
	}
}
