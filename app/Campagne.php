<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campagne extends Model
{
	public function locations()
	{
		return $this->belongsToMany('App\Location');
	}
}
