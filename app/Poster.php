<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poster extends Model
{
    public function campagne()
    {
    	return $this->belongsTo('App\Campagne' );
    }
    public function user()
    {
    	return $this->belongsTo('App\User' );
    }
}
