<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampagneLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('campagne_location', function (Blueprint $table) {
	    	$table->integer('campagne_id')->unsigned();
	    	$table->integer('location_id')->unsigned();
	    	$table->foreign('campagne_id')->references('id')->on('campagnes');
	    	$table->foreign('location_id')->references('id')->on('locations');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campagne_location');
    }
}
