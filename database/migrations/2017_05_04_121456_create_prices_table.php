<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('prices', function (Blueprint $table) {
		    $table->increments( 'id' );
		    $table->string( 'name' );
		    $table->float( 'lease_costs', 8, 2);
		    $table->float( 'rent_costs', 8, 2 );
		    $table->boolean( 'blocked' )->default(false);
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
