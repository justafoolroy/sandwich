<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    if ( Schema::hasTable('prices') )
	    {
		    Schema::table('locations', function( Blueprint $table ) {
			    $table->integer('price_id')->unsigned();
			    $table->foreign('price_id')->references('id')->on('prices');
		    });
	    }
	    if ( Schema::hasTable('posters') )
	    {
	    	Schema::table('posters', function( Blueprint $table ) {
			    $table->integer('user_id')->unsigned();
			    $table->integer('campagne_id' )->unsigned();
			    $table->foreign('campagne_id')->references('id')->on('campagnes');
			    $table->foreign('user_id')->references('id')->on('users');
		    });
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
    }
}
