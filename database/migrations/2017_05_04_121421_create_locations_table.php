<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('locations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('adress');
			$table->float('lattitude', 10, 6 );
			$table->float('longtitude', 10, 6 );
			$table->boolean('blocked')->default(false);
			$table->string('image_path')->default("");
			$table->rememberToken();
			$table->timestamps();
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('locations');
    }
}
